﻿using UnityEngine;
using System.Collections;

public class CharacterBehaviourScript : MonoBehaviour {
	private float 				stamina;
	private CharacterController controller;
	private Camera 				cam;
	private GameObject			turret;
	private GameObject			staminaBar;
	private GameObject			missileValue;
	private	float				lastGunShot = 0f;
	private	float 				lastMissileShot = 0f;
	private	float 				Hp;

	public	float				maxHp = 100f;
	public	float 				speed = 3.0f;
	public	float 				rotateSpeed = 3.0f;
	public	float 				boostMultiplier = 5.0f;
	public	int					missiles = 5;
	public	float 				maxStamina = 100.0f;
	public	GameObject			gunParticule;
	public	GameObject			missileParticule;
	public	GameObject 			explosion;

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		controller = GetComponent<CharacterController>();
		turret = gameObject.transform.FindChild("canon").gameObject;
		staminaBar = GameObject.FindGameObjectWithTag("Stamina");
		missileValue = GameObject.FindGameObjectWithTag("Missile");
		stamina = maxStamina;
		Hp = maxHp;
	}
	
	// Update is called once per frame
	void Update () {
		// Moving tank body
		Vector3 forward = transform.TransformDirection(Vector3.forward);
		transform.Rotate(0f, Input.GetAxis("Horizontal") * rotateSpeed, 0f);
		float curSpeed = speed * Input.GetAxis("Vertical");
		if (Input.GetKey(KeyCode.LeftShift) && stamina >= 10f){
			curSpeed += boostMultiplier;
			stamina = Mathf.Max(stamina - 1, 0f);
		}else{
			stamina = Mathf.Min(stamina + 0.5f, maxStamina);
		}
		controller.SimpleMove(forward * curSpeed);

		// Rotating turret
		turret.transform.Rotate(Vector3.up, Input.GetAxis("Mouse X"));
		turret.transform.Rotate(Vector3.left, Input.GetAxis("Mouse Y"));
		turret.transform.Rotate(Vector3.forward, 0f);

		// Hitting point
		RaycastHit hitInfo;
		Physics.Raycast(cam.transform.position, cam.transform.forward,out hitInfo);
		if (Input.GetMouseButton(0)) {
			if (lastGunShot == 0f || (Time.time - lastGunShot) > 0.2f) {
				lastGunShot = Time.time;
				GameObject.Instantiate (gunParticule).transform.position = hitInfo.point;
				if (hitInfo.collider.tag == "enemy") {
					hitInfo.collider.gameObject.GetComponentInParent<EnemyBehaviorScript> ().TakeDamage (2f);
				}
			}
		}
		if (Input.GetMouseButtonDown(1) && missiles > 0) {

			if (lastMissileShot == 0f || (Time.time - lastMissileShot) > 2f) {
				lastMissileShot = Time.time;
				missiles--;
				GameObject.Instantiate (missileParticule).transform.position = hitInfo.point;
				if (hitInfo.collider.tag == "enemy") {
					hitInfo.collider.gameObject.GetComponentInParent<EnemyBehaviorScript> ().TakeDamage (50f);
				}
			}
		}
	}
	void LateUpdate () {
		staminaBar.transform.localScale = new Vector3 (stamina/maxStamina, 1f, 1f);
		missileValue.GetComponent<UnityEngine.UI.Text>().text = missiles.ToString();
	}

	public	void TakeDamage(float dmg){
		Hp -= dmg;
		if (Hp <= 0f) {
			Debug.Log ("Game Over");
			GameObject.Instantiate (explosion).transform.position = transform.position;
		}
	}
}
