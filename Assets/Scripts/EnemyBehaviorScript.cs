﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class EnemyBehaviorScript : MonoBehaviour {
	private float 				stamina;
	private NavMeshAgent		controller;
	private GameObject			turret;
	private	float				lastGunShot = 0f;
	private	float 				lastMissileShot = 0f;
	private	GameObject			target;

	public	float 				Hp;
	public	float				maxHp = 100f;
	public	float 				speed = 3.0f;
	public	float 				rotateSpeed = 3.0f;
	public	float 				boostMultiplier = 5.0f;
	public	int					missiles = 5;
	public	float 				maxStamina = 100.0f;
	public	GameObject			gunParticule;
	public	GameObject			missileParticule;
	public	GameObject 			explosion;

	// Use this for initialization
	void Start () {
		controller = GetComponent<NavMeshAgent>();
		turret = gameObject.transform.FindChild("canon").gameObject;
		stamina = maxStamina;
		Hp = maxHp;
		target = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void Update () {
		//	checked if target is in sight
//		NavMeshHit navmeshhit;
//		controller.Raycast(target.transform.position,out navmeshhit);
		RaycastHit hitInfo;
		Physics.Raycast(
			turret.transform.position,
			(target.transform.position - turret.transform.position),
			out hitInfo);
		if (hitInfo.collider && (hitInfo.collider.tag == "Body") && (hitInfo.distance <= 100f)) {
			controller.Stop ();
			turret.transform.LookAt (hitInfo.point);

			if (lastMissileShot == 0f || (Time.time - lastMissileShot) > 5f) {
				lastMissileShot = Time.time;
				GameObject.Instantiate (missileParticule).transform.position = hitInfo.point;
				hitInfo.collider.gameObject.GetComponentInParent<CharacterBehaviourScript> ().TakeDamage (20f);
			}
		} else {
			controller.SetDestination (target.transform.position);
		}
		// Moving tank body






//		transform.Rotate(0f, Input.GetAxis("Horizontal") * rotateSpeed, 0f);
//		Vector3 forward = transform.TransformDirection(Vector3.forward);
//		float curSpeed = speed * Input.GetAxis("Vertical");
//		if (Input.GetKey(KeyCode.LeftShift) && stamina >= 10f){
//			curSpeed += boostMultiplier;
//			stamina = Mathf.Max(stamina - 1, 0f);
//		}else{
//			stamina = Mathf.Min(stamina + 0.5f, maxStamina);
//		}
//		controller.SimpleMove(forward * curSpeed);
//
//		// Rotating turret
//		turret.transform.Rotate(Vector3.up, Input.GetAxis("Mouse X"));
//
//		// Hitting point
//		RaycastHit hitInfo;
//		Physics.Raycast(turret.transform.position, turret.transform.forward,out hitInfo);
//		if (Input.GetMouseButton(0)) {
//			if (lastGunShot == 0f || (Time.time - lastGunShot) > 0.2f) {
//				lastGunShot = Time.time;
//				GameObject.Instantiate (gunParticule).transform.position = hitInfo.point;
//			}
//		}
//		if (Input.GetMouseButtonDown(1) && missiles > 0) {
//
//		}
	}
	void LateUpdate () {
	}

	public	void TakeDamage(float dmg){
		Hp -= dmg;
		if (Hp <= 0f) {
			GameObject.Instantiate (explosion).transform.position = transform.position;
			Destroy (gameObject);
		}
	}
}
